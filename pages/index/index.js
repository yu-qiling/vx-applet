// index.js
// 获取应用实例
const app = getApp()

Page({
    
    data: {
        region: ['广东省', '潮州市', '饶平县'],
        key:'d8fd1d25c0f64e29ba65d5e0f0cf0573',//去官网复制你的key噢！
        locationId:'',
        weather:''//我们请求回来的数据
      },
      //这是第一部分，获取用户选择的位置(地区)
  bindRegionChange: function (e) {
    console.log("我们获取到了用户的数据", e.detail.value)
    this.setData({
      region: e.detail.value
    })
    this.getLocationId()

  },

  //第二部分获取到用户选择的地区后利用wx.request去请求locationid
  getLocationId:function(){
    var that = this
    var key = this.data.key
    //调用该函数获取用户选的的地区
    wx.request({
        url: 'https://geoapi.qweather.com/v2/city/lookup?'+ 'location=' + this.data.region[2] + '&key=' + key,
      //如果请求成功
        success: function (res) {
          console.log('获取到了locationid啦',res.data.location[0].id)
          //把请求到的locationId赋值给data中的locationId
            that.setData({
                locationId: res.data.location[0].id
            })
            // //调用获取城市天气的函数
            that.getWeather()//第一我们要调用它，第二我们要有这个函数（that.getWeather is not a function）
        }
    })
  },
  //第三部分：通过key和locationId，请求和风天气的城市数据
  getWeather:function(){
    //获取指针
    var that = this
    //这是我们的和风天气复制过来的key
    //再复习一下，我们请求天气需要什么？ 1.key，2.城市名字（城市名字是通过picker选择的）=> locationid
    var key = that.data.key
    var locationId = that.data.locationId
    console.log("太好啦，我们已经有key--->" + key)
    console.log("太好啦，我们已经有经纬度--->" + locationId)
    //请求天气数据
    wx.request({
      url: 'https://devapi.qweather.com/v7/weather/now?' + 'location=' + locationId + '&key=' + key,
      //如果请求成功
      success: function (res) {
        //把请求到的天气数据赋值给data中的weather
        that.setData({
          weather: res.data.now//我们接受天气的数据并且存放在我们的weather变量里面
        })
        console.log(res.data.now)
      }
    })
  },
    //页面加载
onLoad:function(options){
    //每次打开页面调用这个函数然后获取从化区的天气
    this.getLocationId();
  },

})
